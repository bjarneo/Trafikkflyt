Trafikkflyt
===========

Mobile web application based on API from Trafikkflyt and Google Maps.<br />
<a href="http://trafikkflyt.nyhetsportal.no">http://trafikkflyt.nyhetsportal.no</a>

### Technologies used
* JavaScript
* PHP
* Slim framework (php)
* NodeJS
* Grunt

### Updates:

#### v 1.2.1
* appendHTML() bugfix.
* Removed requirejs, this app doesn't need module loader.
* Combined trafikkfly.min.js and help.min.js to one file app.min.js
* toggle() added.
* css() added.
* Removed jQuery.

#### v 1.1.0
* help.js is chainable. ex: help('#id').fadeOut().fadeIn().show().hide()
* h.ajax() for ajax
* addEvent method is now bind
* removed yep nope
* added requirejs
* grunt added

#### v 1.0.0
* Release