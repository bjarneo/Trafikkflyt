<!DOCTYPE html>
<html>
<head>
    <title>Trafikkflyt</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="apple-touch-icon" sizes="144x144" href="assets/images/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="120x120" href="assets/images/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="114x114" href="assets/images/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="72x72" href="assets/images/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" href="assets/images/apple-touch-icon.png">
    <link rel="icon" type="image/x-icon" href="assets/images/favicon.png">
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400" rel="stylesheet" type="text/css">
    <link href="assets/css/trafikkflyt.min.css" rel="stylesheet" media="screen">
    <script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false"></script>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-29878077-7', 'nyhetsportal.no');
        ga('send', 'pageview');
    </script>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-12 content-bg">
                <select id="county">
                    <?php foreach($counties as $_name => $_id): ?>
                        <option value="<?php echo $_id; ?>"><?php echo $_name; ?></option>
                    <?php endforeach; ?>
                </select>
                <button type="button" class="btn btn-ttc btn-lg get-traffic">Hent trafikk</button>
            </div>
            <div class="col-md-12 content-bg mapwrapper">
                <h4 class="toggle-map">Din posisjon <span class="glyphicon glyphicon-map-marker pull-right"></span></h4>
                <div id="mapcanvas">Google maps laster..</div>
            </div>
            <div class="col-md-12 preloader">
                <img src="assets/images/preloader.gif" />
            </div>
        </div>
        <div class="row row-traffic"></div>
        <div class="row">
            <div class="col-md-12 content-bg">
                <small>Lagd av <a href="http://www.nyhetsportal.no" target="_blank">Nyhetsportal.no</a> | Basert på API fra <a href="http://www.trafikkflyt.no">Trafikkflyt.no</a></small>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="assets/js/app.min.js"></script>
    </body>
</html>