<?php
require 'Model/Trafikkflyt.php';
require 'Slim/Slim.php';
\Slim\Slim::registerAutoloader();
$trafikk = new Trafikkflyt();
$app = new \Slim\Slim(array(
    //'debug' => true,
    //'log.level' => \Slim\Log::DEBUG,
    'templates.path' => './view',
    'mode' => 'production'
));

// GET route
$app->get('/', function () use ($app, $trafikk) {
    $app->etag(uniqid());
    $app->expires('+60 minutes');
    $app->render('trafikkflyt.php', array(
        'counties' => $trafikk->county_id
    ));
});

// GET google
/* Currently not in use
$app->get('/google/:latlng', function ($latlng) use($trafikk) {
    //Your latitude: 59.9466146
    //Your longitude: 11.011411
    $trafikk->get_feed_by_geolocation($latlng);
});
*/

// Get feed - TODO: password protection
$app->get('/trafikk/:area', function ($area) use($app, $trafikk) {
    $app->response->headers->set('Content-Type', 'application/json');
    $trafikk->get_trafikkflyt_feed_by_areacode($area);
});

$app->run();
