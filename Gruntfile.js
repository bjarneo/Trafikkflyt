module.exports = function(grunt){
    require("matchdep").filterDev("grunt-*").forEach(grunt.loadNpmTasks);
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        htmlhint: {
            build: {
                options: {
                    'tag-pair': true,
                    'tagname-lowercase': true,
                    'attr-lowercase': true,
                    'attr-value-double-quotes': true,
                    'doctype-first': true,
                    'spec-char-escape': true,
                    'id-unique': true,
                    'head-script-disabled': true,
                    'style-disabled': true
                },
                src: ['view/trafikkflyt.php']
            }
        },
        jshint: {
            all: ['assets/js/Trafikkflyt.js', 'assets/js/help.js']
        },
        uglify: {
            build: {
                files: {
                    'assets/js/trafikkflyt.min.js': ['assets/js/Trafikkflyt.js'],
                    'assets/js/help.min.js': ['assets/js/help.js'],
                    'assets/js/app.min.js': ['assets/js/help.js', 'assets/js/Trafikkflyt.js']
                }
            }
        },
        cssmin: {
            combine: {
                files: {
                    'assets/css/trafikkflyt.min.css': ['assets/css/bootstrap.min.css', 'assets/css/trafikkflyt.css']
                }
            }
        },
        watch: {
             html: {
                files: ['view/trafikkflyt.php'],
                tasks: ['htmlhint']
             },
            js: {
                files: ['assets/js/Trafikkflyt.js', 'assets/js/help.js'],
                tasks: ['uglify', 'jshint']
            },
            css: {
                files: ['assets/css/bootstrap.min.css', 'assets/css/trafikkflyt.css'],
                tasks: ['cssmin']
            }
        }

    });

    grunt.registerTask('default', []);

};