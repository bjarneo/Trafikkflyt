<?php
/**
 * Based on trafikkflyt.no and Google API.
 * @author: Bjarne Oeverli (@bjarneo_)
 * @website: http://www.codephun.com
 */

class Trafikkflyt
{
    public $county_id;
    protected $_trafikkflyt_api;
    protected $_trafikkflyt_area;
    protected $_google_api;
    protected $_cache_dir;
    protected $_areacode;

    public function __construct()
    {
        $this->_cache_dir = dirname(__DIR__) . '/cache/';
        $this->_trafikkflyt_api = 'http://www.p4.no/rss/trafikk.aspx';
        $this->_trafikkflyt_area = $this->_trafikkflyt_api . '?area=';
        $this->_google_api = 'http://maps.googleapis.com/maps/api/geocode/json?';
        $this->county_id = array(
            'Akershus' => 8,
            'Aust-Agder' => 17,
            'Buskerud' => 12,
            'Finnmark' => 21,
            'Hedmark' => 7,
            'Hordaland' => 11,
            'Møre og Romsdal' => 25,
            'Nordland' => 22,
            'Nord-Trøndelag' => 24,
            'Østfold' => 16,
            'Oppland' => 10,
            'Oslo' => 13,
            'Rogaland' => 18,
            'Sogn og Fjordane' => 26,
            'Sør-Trøndelag' => 14,
            'Telemark' => 19,
            'Troms' => 23,
            'Vest-Agder' => 20,
            'Vestfold' => 15
        );
    }

    /**
     * For geolocation !! Currently not in use!
     * @param $latlng
     * @return bool|json
     */
    public function get_feed_by_geolocation($latlng)
    {
        if(!isset($latlng)) {
            return false;
        }

        $feed = file_get_contents($this->_google_api . "latlng=$latlng&sensor=true");
        if($feed) {
            $json = json_decode($feed);

            foreach($json->results[0]->address_components as $_county) {
                if(array_key_exists($_county->short_name, $this->county_id)) {
                    echo json_encode(array(
                        'feed' => $this->get_trafikkflyt_feed_by_areacode($_county->short_name)
                    ));
                }
            }
        }

    }

    /**
     * Get json feed by areacode
     * @param $areacode
     * @return bool
     */
    public function get_trafikkflyt_feed_by_areacode($areacode)
    {
        $json = array();

        if(!isset($areacode)) {
            return false;
        }
        $this->_areacode = $areacode;

        if(!$cache = $this->_cache_exists()) {
            $json = $this->_trafikkflyt_api();
            $this->_cache($json);
            echo $json;
        } else {
            echo $cache;
        }
    }

    /**
     * Create jsonm by xml-feed based on area code from Trafikkflyt
     * @return json
     */
    protected function _trafikkflyt_api()
    {
        $feed = file_get_contents($this->_trafikkflyt_area . $this->_areacode);
        if($feed) {
            $xml = simplexml_load_string($feed);
            foreach($xml->channel as $_items) {
                $i = 0;
                $json['title'] = (string)$_items->title;
                foreach($_items->item as $_item) {
                    if($i === 10) {
                        break;
                    }

                    $json[] = array(
                        'title' => htmlentities((string)$_item->title, ENT_QUOTES, 'UTF-8'),
                        'link' => htmlentities((string)$_item->link, ENT_QUOTES, 'UTF-8'),
                        'pubDate' => str_replace(array(
                                'January', 'February', 'March',
                                'May', 'June', 'July',
                                'October', 'December'
                            ), array(
                                'Januar', 'Februar', 'Mars',
                                'Mai', 'Juni', 'Juli',
                                'Oktober', 'Desember'
                            ), date('d F Y H:i', strtotime((string)$_item->pubDate))
                        )
                    );

                    $i++;
                }
                $json['len'] = $i;
            }

            return json_encode($json);
        }
    }

    /**
     * Cache file
     * @param $feed
     */
    protected function _cache($feed)
    {
        $filename = $this->_cache_dir . $this->_areacode . '.json';
        $cache = file_put_contents($filename, $feed);
    }

    /**
     * If cache exists and is younger than 15 minutes return cache.
     * @return json
     */
    protected function _cache_exists()
    {
        $files = array();
        $dir = new DirectoryIterator($this->_cache_dir);

        foreach($dir as $file) {
            if($file->isFile()) {
                $files[$file->getFilename()] = $file->getPathname();
                if(($file->getFilename() === $this->_areacode . '.json') && (strtotime('+15 minutes', $file->getMTime())) > strtotime('now')) {
                    return file_get_contents($file->getPathname());
                }
            }
        }
    }
}
