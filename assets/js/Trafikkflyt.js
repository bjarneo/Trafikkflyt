/**
 * Trafikkflyt
 * Does not support IE < 9
 * @author: Bjarne Øverli
 * @website: http://www.codephun.com
 * @twitter: @bjarneo_
 */
var Trafikkflyt =
{
    // Preloader
    preloader: help('.preloader'),

    feedApi: 'index.php/trafikk/',

    // Init
    init: function() {
        var geo = navigator.geolocation,
            map = help('#mapcanvas'),
            // Use self to hold object. Now we don't need to use 'this', because 'this' won't work in callback functions
            self = this;

        if(!geo) {
            help('.mapwrapper').hide();
        } else {
            // Show map of current pos
            geo.getCurrentPosition(
                self.currentPosition,
                self.geoError,
                {
                    enableHighAccuracy: true,
                    timeout: 7500,
                    maximumAge: 0
                }
            );
        }

        // Get list
        help('.get-traffic').bind('click', function() {
            var county = document.getElementById('county');
            map.fadeOut();
            self.getTraffic(county.options[county.selectedIndex].value);
        });

        // toggle map
        help('.toggle-map').bind('click', function() {
            map.toggle('fast');
        });
    },

    // Current pos based on geolocation
    currentPosition: function(position) {
        var latlng = new google.maps.LatLng(
            position.coords.latitude,
            position.coords.longitude
        );

        var map = new google.maps.Map(
            document.getElementById('mapcanvas'),
            {
                zoom: 15,
                center: latlng,
                mapTypeControl: false,
                navigationControlOptions: {style: google.maps.NavigationControlStyle.SMALL},
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }
        );

        var marker = new google.maps.Marker({
            position: latlng,
            map: map,
            title:"Din plassering. (innenfor " + position.coords.accuracy + " meter radius)"
        });
    },

    // Get Trafikkflyt feed by area code
    getTraffic: function(areacode) {
        var trafficFeed = help('.traffic-feed'),
            self = this;

        self.preloader.show();

        if(trafficFeed.length) {
            trafficFeed.remove();
        }

        h.ajax({
            type: 'GET',
            dataType: 'JSON',
            url: self.feedApi + areacode,
            success: function (data) {
                self.preloader.hide();
                self.parseFeed(data);
            }
        });
    },

    // Parse data to dom
    parseFeed: function(data) {
        var i = 0,
            len = data.len;

        // appending html via js is bad practice, maybe change to RactiveJS framework later.
        for(; i < len; i++) {
            if(i === 0) {
                help('.row-traffic')
                    .appendHTML(
                        '<div class="col-md-12 content-bg traffic-feed title">' +
                            '<h4>' + data.title + '</h4>' +
                        '</div>'
                    );
            }
            help('.row-traffic')
                .appendHTML(
                    '<div class="col-md-12 content-bg traffic-feed">' +
                        '<p>' + data[i].title +'</p>' +
                        '<p class="nomargin"><span class="glyphicon glyphicon-time"></span> ' + data[i].pubDate +
                            '<a href="' + data[i].link + '" class="pull-right" target="_blank">' +
                                '<span class="glyphicon glyphicon-link"></span>' +
                            '</a>'+
                        '</p>' +
                    '</div>'
                );
        }
    },

    // Geolocation error
    geoError: function() {
        help('.mapwrapper').hide();
    }
};

// Initialize Trafikkflyt JS.
Trafikkflyt.init();